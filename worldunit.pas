{Copyright (C) 2012-2017 Yevhen Loza

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.}

{---------------------------------------------------------------------------}

(* Constructs and manages the world *)

unit WorldUnit;

{$mode objfpc}{$H+}

interface

uses
  SysUtils, CastleWindow, CastleSceneCore, CastleScene,
  CastleLog, X3DNodes, CastleVectors, CastleViewport;

var
  Scene: TCastleScene;

procedure PrepareScene;

implementation

uses
  WindowUnit, PlayerUnit, MapUnit;

procedure PrepareScene;
var
  GenerationNode: TX3DRootNode;
  Nav: TKambiNavigationInfoNode;
  NavLight: TPointLightNode; {$HINT Check this}

  Viewpoint: TViewpointNode;
  Viewport: TCastleViewport;
begin
  Player := TPlayer.Create;

  Viewport := TCastleViewport.Create(Window);
  Viewport.FullSize := true;
  Viewport.Navigation := Player.Navigation;
  //Player.Navigation.InternalViewport := Viewport;
  Window.Controls.InsertFront(Viewport);

  Scene := TCastleScene.Create(Application);
  Scene.PreciseCollisions := false;
  Scene.ProcessEvents := true;

  Location := TLocationGenerator.Create;
  Location.EntranceX := 1;
  Location.EntranceY := MapSizeY div 2;
  Location.MakeMap(LZebranky);
  GenerationNode := Location.MakeRoot;
  Minimap := Location.MakeMinimap;
  Player.Teleport(Location.EntranceX, Location.EntranceY, drSouth);
  Location.Free;

  //create light that follows the player
  NavLight := TPointLightNode.Create;
  NavLight.FdColor.Value := Vector3(1, 0.3, 0.1);
  NavLight.FdAttenuation.Value := Vector3(1, 0, 1);
  NavLight.FdRadius.Value := 10 * 3;
  NavLight.FdIntensity.Value := 20 * 3;
  NavLight.FdOn.Value := True;
  NavLight.FdShadows.Value := False;

  Nav := TKambiNavigationInfoNode.Create;
  Nav.FdHeadLightNode.Value := NavLight;
  Nav.FdHeadlight.Value := true;
  GenerationNode.FdChildren.Add(Nav);

  Viewpoint := TViewpointNode.Create;
  Viewpoint.FieldOfView := 1.3;     // THIS NO LONGER DOES ANYTHING
  GenerationNode.FdChildren.Add(Viewpoint);

  Scene.Load(GenerationNode, true);
  //Scene.DistanceCulling := 30;

  //Save3D(GenerationNode, 'duungeeon.x3d');

  Viewport.Items.Add(Scene);
  Viewport.Items.MainScene := Scene;
  Viewport.Camera.Perspective.FieldOfView := 1.3;
end;

end.

